ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"
val Http4sVersion = "0.23.10"

Test / parallelExecution := false

lazy val root = (project in file("."))
  .settings(
    name := "api",
    libraryDependencies ++= Seq(
      "org.json4s" %% "json4s-jackson" % "4.0.4",
      "org.xerial" % "sqlite-jdbc" % "3.36.0.2",
      "io.getquill" %% "quill-jdbc" % "3.12.0",
      "org.scalatest" % "scalatest_2.13" % "3.1.0" % "test",
      "org.slf4j" % "slf4j-nop" % "1.7.35" % Test,
      "org.scalatest" % "scalatest_2.13" % "3.1.0" % "test",
//      "ch.qos.logback" % "logback-classic" % "1.2.10",
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.json4s" %% "json4s-jackson" % "4.0.4"
    )
  )

