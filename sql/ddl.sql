CREATE TABLE countries (
--     country_id INTEGER PRIMARY KEY NOT NULL,
    country_code VARCHAR(10) PRIMARY KEY UNIQUE NOT NULL,
    name VARCHAR(100) NOT NULL,
    continent VARCHAR(2) NOT NULL,
    wiki_link VARCHAR(250) NOT NULL,
    keywords BLOB NOT NULL
);

CREATE TABLE airports (
--     airport_id INTEGER PRIMARY KEY NOT NULL,
	ident_airport VARCHAR(10) PRIMARY KEY NOT NULL,
    country_code VARCHAR(10) NOT NULL,
	type_airport VARCHAR(50) NOT NULL,
	name VARCHAR(100) NOT NULL,
    continent_code VARCHAR(2),
    region_code VARCHAR(10),
	latitude_deg DECIMAL NOT NULL,
	longitude_deg DECIMAL NOT NULL,
	elevation_ft INTEGER,
	municipality VARCHAR(100),
	scheduled_service BOOLEAN NOT NULL,
	gps_code VARCHAR(5),
	iata_code VARCHAR(5),
	local_code VARCHAR(5),
	home_link VARCHAR(250),
	wiki_link VARCHAR(250),
	keywords BLOB NOT NULL,

	CONSTRAINT fk_country FOREIGN KEY (country_code) REFERENCES countries(country_code)
);

CREATE TABLE runways (
	runway_id INTEGER PRIMARY KEY NOT NULL,
-- 	airport_id INTEGER NOT NULL,
    ident_airport VARCHAR(10) NOT NULL,
	length_ft INTEGER NOT NULL,
	width_ft INTEGER NOT NULL,
	surface VARCHAR(10) NOT NULL,
	lighted BOOLEAN NOT NULL,
	closed BOOLEAN NOT NULL, 
	le_ident VARCHAR(5) NOT NULL,
	le_latitude_deg DECIMAL,
	le_longitude_deg DECIMAL,
	le_elevation_ft INTEGER,
    le_heading_deg_t DECIMAL,
    le_displaced_threshold_ft INTEGER,
	he_ident VARCHAR(5),
	he_latitude_deg DECIMAL,
	he_longitude_deg DECIMAL,
	he_elevation_ft INTEGER,
    he_heading_deg_t DECIMAL,
    he_displaced_threshold_ft INTEGER,

	CONSTRAINT fk_airport FOREIGN KEY (ident_airport) REFERENCES airports(ident_airport)
);