package models

import org.scalatest.{BeforeAndAfter, BeforeAndAfterAllConfigMap, ConfigMap}
import org.scalatest.funsuite.AnyFunSuite
import repositories.CountriesRepository
import utils.CSV

// https://www.scalatest.org/scaladoc/3.2.9/org/scalatest/BeforeAndAfterAllConfigMap.html
class CountriesTest extends AnyFunSuite with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  override def beforeAll(configMap: ConfigMap): Unit = {
    System.setProperty("ctx.jdbcUrl", "jdbc:sqlite:test/test.db")
  }
  before {
    CountriesRepository.deleteAll()
  }

  after {
    CountriesRepository.deleteAll()
  }

  test("CSV parsing") {
    val result = CSV.read("countries.csv", Countries.parseCountry)
    assert(result.nbInvalidLine == 1)
    assert(result.lines.size == 246)
  }

  test("Mass insertion") {
    val result = CountriesRepository.batchInsert(CSV.read("countries.csv", Countries.parseCountry).lines.toList)
    assert(result.size == 246)
  }
}
