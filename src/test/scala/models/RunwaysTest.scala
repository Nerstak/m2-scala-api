package models

import org.scalatest.{BeforeAndAfter, BeforeAndAfterAllConfigMap, ConfigMap}
import org.scalatest.funsuite.AnyFunSuite
import repositories.{AirportsRepository, CountriesRepository, RunwaysRepository}
import utils.CSV

class RunwaysTest extends AnyFunSuite with BeforeAndAfter  with BeforeAndAfterAllConfigMap {
  override def beforeAll(configMap: ConfigMap): Unit = {
    System.setProperty("ctx.jdbcUrl", "jdbc:sqlite:test/test.db")
  }

  before {
    RunwaysRepository.deleteAll()
    AirportsRepository.deleteAll()
    CountriesRepository.deleteAll()

    CountriesRepository.batchInsert(CSV.read("countries.csv", Countries.parseCountry).lines.toList)
    AirportsRepository.batchInsert(CSV.read("airports.csv", Airports.parseAirport).lines.toList)
  }

  after {
    RunwaysRepository.deleteAll()
    AirportsRepository.deleteAll()
    CountriesRepository.deleteAll()
  }

  test("CSV parsing") {
    val result = CSV.read("runways.csv", Runways.parseRunway)
    assert(result.nbInvalidLine == 2568)
    assert(result.lines.size == 36968)
  }

  test("Mass insertion") {
    val result = RunwaysRepository.batchInsert(CSV.read("runways.csv", Runways.parseRunway).lines.toList)
    assert(result.size == 36968)
  }
}
