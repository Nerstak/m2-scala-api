package models


import org.scalatest.{BeforeAndAfter, BeforeAndAfterAllConfigMap, BeforeAndAfterEach, ConfigMap}
import org.scalatest.funsuite.AnyFunSuite
import repositories.{AirportsRepository, CountriesRepository, RunwaysRepository}
import utils.CSV

class AirportsTest extends AnyFunSuite with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  override def beforeAll(configMap: ConfigMap): Unit = {
    System.setProperty("ctx.jdbcUrl", "jdbc:sqlite:test/test.db")
  }

  before {
    AirportsRepository.deleteAll()
    CountriesRepository.deleteAll()
    CountriesRepository.batchInsert(CSV.read("countries.csv", Countries.parseCountry).lines.toList)
  }

  after {
    AirportsRepository.deleteAll()
    CountriesRepository.deleteAll()
  }

  test("CSV parsing") {
    val result = CSV.read("airports.csv", Airports.parseAirport)
    assert(result.nbInvalidLine == 21)
    assert(result.lines.size == 46484)
  }

  test("Mass insertion") {
    val result = AirportsRepository.batchInsert(CSV.read("airports.csv", Airports.parseAirport).lines.toList)
    assert(result.size == 46484)
  }
}
