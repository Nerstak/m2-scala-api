package services

import models._
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAllConfigMap, ConfigMap}
import repositories.{AirportsRepository, CountriesRepository, RunwaysRepository}
import service.{CountryRunWayTypeDto, ReportService}
import utils.CSV

class ReportServiceTest extends AnyFunSuite with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  override def beforeAll(configMap: ConfigMap): Unit = {
    System.setProperty("ctx.jdbcUrl", "jdbc:sqlite:test/test.db")
  }

  before {
    RunwaysRepository.deleteAll()
    AirportsRepository.deleteAll()
    CountriesRepository.deleteAll()

    CountriesRepository.batchInsert(CSV.read("countries.csv", Countries.parseCountry).lines.toList)
    AirportsRepository.batchInsert(CSV.read("airports.csv", Airports.parseAirport).lines.toList)
    RunwaysRepository.batchInsert(CSV.read("runways.csv", Runways.parseRunway).lines.toList)
  }

  after {
    RunwaysRepository.deleteAll()
    AirportsRepository.deleteAll()
    CountriesRepository.deleteAll()
  }

  test("findCountriesWithMostAirports"){
    val res = ReportService.findCountriesWithMostAirports()
    assert(res.head.count == 21484)
    assert(res.head.country == "United States")
    assert(res(1).count == 3839)

  }

  test("findCountriesWithLeastAirports"){
    val res = ReportService.findCountriesWithLeastAirports()
    assert(res.head.count  == 1)
    assert(res.head.country  == "Andorra")
  }

  test("findTypeOfRunwaysPerCountry"){
    val res = ReportService.findTypeOfRunwaysPerCountry(CountryCode("US"))
    val expect = CountryRunWayTypeDto(
      "United States",
      List(
        RunwayType("U"),
        RunwayType("ASP"),
        RunwayType("BIT"),
        RunwayType("GVL"),
        RunwayType("CON"),
        RunwayType("GRVL"),
        RunwayType("COM"),
        RunwayType("TURF"),
        RunwayType("GRE"),
        RunwayType("GRS"),
        RunwayType("PSP"),
        RunwayType("DIRT"),
        RunwayType("GRAVEL"),
        RunwayType("PEM"),
        RunwayType("COP"),
        RunwayType("PER"),
        RunwayType("WAT"),
        RunwayType("LAT"),
        RunwayType("GRASS"),
        RunwayType("SAND"),
        RunwayType("ICE"),
        RunwayType("MATS")
      )
    )
    assert(res.isDefined)
    assert(res.get == expect)
  }

  test("findTop10MostCommonLatitude"){
    val res = ReportService.findTop10MostCommonLatitude();
    assert(List(
      ("H1", 5566),
      ("18", 2943),
      ("09", 2305),
      ("17", 2218),
      ("16", 1462),
      ("12", 1342),
      ("13", 1338),
      ("14", 1335),
      ("08", 1310),
      ("15", 1285)
    ).equals(res))
  }
}
