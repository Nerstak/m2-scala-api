package services

import models.{AirportCode, Airports, Countries, CountryCode, Runways}
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAllConfigMap, ConfigMap}
import org.scalatest.funsuite.AnyFunSuite
import repositories.{AirportsRepository, CountriesRepository, RunwaysRepository}
import service.QueryService
import utils.CSV

class QueryServiceTest extends AnyFunSuite with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  override def beforeAll(configMap: ConfigMap): Unit = {
    System.setProperty("ctx.jdbcUrl", "jdbc:sqlite:test/test.db")
  }

  before {
    RunwaysRepository.deleteAll()
    AirportsRepository.deleteAll()
    CountriesRepository.deleteAll()

    CountriesRepository.batchInsert(CSV.read("countries.csv", Countries.parseCountry).lines.toList)
    AirportsRepository.batchInsert(CSV.read("airports.csv", Airports.parseAirport).lines.toList)
    RunwaysRepository.batchInsert(CSV.read("runways.csv", Runways.parseRunway).lines.toList)
  }

  after {
    RunwaysRepository.deleteAll()
    AirportsRepository.deleteAll()
    CountriesRepository.deleteAll()
  }

  test("findAirportAndRunwaysByCountryName"){
    val res = QueryService.findAirportAndRunwaysByCountryName("United States")
    assert(res.iterator.size  == 23723);
    assert(
      res
        .iterator
        .toList
        .count(
          _._1.identAirport.equals(AirportCode("00A"))
        ) == 1
    )
  }

  test("findAirportAndRunwaysByCountryCode"){
    val res = QueryService.findAirportAndRunwaysByCountryCode(Some(CountryCode("US")))
    assert(res.iterator.size  == 23723);
    assert(res
      .iterator
      .toList
      .count(
        _._1.identAirport.equals(AirportCode("00A"))
      ) == 1
    )
  }
}
