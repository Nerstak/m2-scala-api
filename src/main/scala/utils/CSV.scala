package utils

import java.nio.file.{Files, Path}
import scala.jdk.CollectionConverters.IteratorHasAsScala

final case class ReadResult[A](lines: Iterator[A], nbInvalidLine: Int)

object CSV {
  // Took this random regex from there: https://stackoverflow.com/a/18893443
  // Basically match all comma that are not in quotes (quotes need to be balanced)
  def read[A](fileName: String, parseLine: Array[String] => Option[A], regex: String = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"): ReadResult[A] = {
    val (parsedLine, invalidLine) = Option(Files.lines(Path.of(s"data/$fileName")))
      .map(_.iterator().asScala)
      .getOrElse(Iterator.empty) // if file can't be read option will be a none.
      .drop(1)  // drop csv header
      .map(_.split(regex))
      .map(_.map(_.trim))
      .map(_.map(_.replaceAll("^\"|\"$", ""))) // Removing quotes
      .map(parseLine)
      .partition(_.isDefined)

    ReadResult(parsedLine.flatten, invalidLine.size)
  }
}