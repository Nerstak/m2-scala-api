package utils

import java.net.URL
import scala.language.implicitConversions
import scala.util.Try

object Helpers {
  implicit def stringToURL(s: String): URL = new URL(s)

  implicit def optionStringToOptionURL(option: Option[String]): Option[URL] = option.flatMap(x => Some(stringToURL(x)))

  def emptyStringOption(string: String): Option[String] = if (string.isEmpty) None else Some(string)

  def toURLOption(string: String): Option[URL] = Try(new URL(string)).toOption

  def stringOptionToList(string: Option[String], sep: String = ","): List[String] = string match {
    case Some(value) => value.split(sep).toList
    case _ => List()
  }
}
