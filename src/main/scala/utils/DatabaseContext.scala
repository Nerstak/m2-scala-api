package utils

import io.getquill.{SnakeCase, SqliteJdbcContext}

object DatabaseContext {
  val ctx = new SqliteJdbcContext(SnakeCase, "ctx") with CustomEncoding
}
