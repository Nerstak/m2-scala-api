package utils

import io.getquill.{SnakeCase, SqliteJdbcContext}

import java.net.URL
import java.sql.Types

trait CustomEncoding {
  // Line below is stolen from there: https://github.com/zio/zio-quill/blob/19cfc85b620b4a0c58a454bf4779496611a599a6/quill-jdbc/src/main/scala/io/getquill/context/jdbc/Decoders.scala
  this: SqliteJdbcContext[SnakeCase.type] =>

  import Helpers._

  implicit val blobEncoder: Encoder[List[String]] = encoder(Types.BLOB, (i, list, row) => {
    row.setObject(i, list.mkString(","))
  })

  implicit val blobDecoder: Decoder[List[String]] = decoder((index, row, _) => row.getObject(index).toString.split(",").toList)


  implicit val urlEncoder: Encoder[URL] = encoder(Types.VARCHAR, (i, x, row) => {
  row.setObject(i, x.toString)
  })

  implicit val urlDecoder: Decoder[URL] = decoder((index, row, _) => row.getObject(index).toString)
}

