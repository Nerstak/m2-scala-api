package service

import models.{Airports, CountryCode, Runways}
import repositories.{AirportsRepository, CountriesRepository}


case class AirPortRunwayDto(airport:String,runways: List[String])
object QueryService {
  def findAirportAndRunwaysByCountryName(countryName: String): IterableOnce[(Airports, Runways)] = {
    CountriesRepository.findCountryByName(countryName) match {
      case country if country.nonEmpty =>
        AirportsRepository.findWithRunways(
          country.head.countryCode
        )
      case _ => None;
    }
  }

  def findAirportAndRunwaysByCountryCode(countryCode: Option[CountryCode]): IterableOnce[(Airports, Runways)] = {
    countryCode match {
      case Some(countryCode) => findByCountryCode(countryCode)
      case _ => None
    }
  }

  private def findByCountryCode(countryCode: CountryCode): IterableOnce[(Airports, Runways)] = {
    CountriesRepository.findByID(countryCode) match {
      case Some(countryCode) => AirportsRepository.findWithRunways(countryCode.countryCode)
      case _ => None
    }
  }
}

