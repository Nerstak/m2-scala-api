package service

import models.{CountryCode, RunwayType}
import repositories.{CountriesRepository, RunwaysRepository}

case class CountryAirportDto(country:String, count:Long)
case class CountryRunWayTypeDto(country:String, runwaysTypes: List[RunwayType])
object ReportService {

  def findCountriesWithMostAirports(): List[CountryAirportDto] = {
    CountriesRepository.findCountriesWithMostAirports()
  }

  def findCountriesWithLeastAirports(): List[CountryAirportDto] = {
    CountriesRepository.findCountriesWithLeastAirports()
  }

  def findTypeOfRunwaysPerCountry(countryCode: CountryCode): Option[CountryRunWayTypeDto] ={
     CountriesRepository
       .findByID(countryCode)
       .map(country =>
         CountryRunWayTypeDto(
           country.name,
           findAllRunwaysTypesOfCountry(country.countryCode)
         )
       )
  }

  def findTop10MostCommonLatitude(): List[(String, Long)] = {
    RunwaysRepository.findMostCommonLatitude()
  }

  private def findAllRunwaysTypesOfCountry(countryCode:CountryCode) : List[RunwayType] =
    RunwaysRepository
      .findRunwaysByCountryId(countryCode)
      .distinct
}
