package models


case class ContinentCode(code: String) extends AnyVal

object ContinentCode {
  private val validCodes = List(
    "AF", // Africa
    "NA", // North America
    "OC", // Oceania
    "AN", // Antarctica
    "AS", // Asia
    "EU", // Europe
    "SA" // South America
  )

  def verify(code: String): Option[ContinentCode] = {
    Some(code).filter(validCodes.contains).map(x => ContinentCode(x))
  }
}
