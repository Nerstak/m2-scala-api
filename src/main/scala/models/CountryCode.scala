package models

import java.util.Locale

case class CountryCode(code: String) extends AnyVal

object CountryCode {
  def verify(code: String): Option[CountryCode] = {
    if (Locale.getISOCountries.find(_ == code).getOrElse("").nonEmpty || code.equals("ZZ")) {
      Some(CountryCode(code))
    } else {
      None
    }
  }
}
