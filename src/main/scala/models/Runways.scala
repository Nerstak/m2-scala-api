package models

import repositories.AirportsRepository
import utils.Helpers.emptyStringOption

import scala.language.implicitConversions

case class
Runways(
                  runwayId: RunwayID,
                  ident_airport: AirportCode,
                  lengthFt: Int,
                  widthFt: Int,
                  surface: RunwayType,
                  lighted: Boolean,
                  closed: Boolean,
                  leIdent: String,
                  heIdent: Option[String],
                  leLatitudeDeg: Option[Double] = None,
                  leLongitudeDeg: Option[Double] = None,
                  leElevationFt: Option[Int] = None,
                  leHeadingDegT: Option[Double] = None,
                  leDisplacedThresholdFt: Option[Int] = None,
                  heLatitudeDeg: Option[Double] = None,
                  heLongitudeDeg: Option[Double] = None,
                  heElevationFt: Option[Int] = None,
                  heHeadingDegT: Option[Double] = None,
                  heDisplacedThresholdFt: Option[Int] = None,
                  )

object Runways {
  implicit def int2bool(b:Int): Boolean = b == 1

  def parseRunway(line: Array[String]): Option[Runways] = {
    (
      line.headOption.flatMap(_.toIntOption), // Runway ID
      verifyAirport(line.lift(2)), // Airport Code
      line.lift(3).flatMap(_.toIntOption), // Length Ft
      line.lift(4).flatMap(_.toIntOption), // Width Ft
      RunwayType.verify(line.lift(5)), // Surface
      line.lift(6).flatMap(_.toIntOption), // Lighted
      line.lift(7).flatMap(_.toIntOption), // Closed
      line.lift(8).flatMap(emptyStringOption), // LE Ident
      line.lift(9).flatMap(_.toDoubleOption), // LE Latitude Deg
      line.lift(10).flatMap(_.toDoubleOption), // LE Longitude Deg
      line.lift(11).flatMap(_.toIntOption), // LE Elevation Ft
      line.lift(12).flatMap(_.toDoubleOption), // LE Heading DegT
      line.lift(13).flatMap(_.toIntOption), // LE Displaced Threshold Ft
      line.lift(14).flatMap(emptyStringOption), // HE Ident
      line.lift(15).flatMap(_.toDoubleOption), // HE Latitude Deg
      line.lift(16).flatMap(_.toDoubleOption), // HE Longitude Deg
      line.lift(17).flatMap(_.toIntOption), // HE Elevation Ft
      line.lift(18).flatMap(_.toDoubleOption), // HE Heading DegT
      line.lift(19).flatMap(_.toIntOption), // HE Displaced Threshold Ft
    ) match {
      case (
        Some(runwayId),
        Some(airportCode),
        Some(lengthFt),
        Some(widthFt),
        surface,
        Some(lighted),
        Some(closed),
        Some(leIdent),
        leLatitudeDeg,
        leLongitudeDeg,
        leElevationFt,
        leHeadingDegT,
        leDisplacedThresholdFt,
        heIdent,
        heLatitudeDeg,
        heLongitudeDeg,
        heElevationFt,
        heHeadingDegT,
        heDisplacedThresholdFt,
        ) => Some(Runways(
          RunwayID(runwayId),
          airportCode,
          lengthFt,
          widthFt,
          surface,
          lighted,
          closed,
          leIdent,
          heIdent,
          leLatitudeDeg,
          leLongitudeDeg,
          leElevationFt,
          leHeadingDegT,
          leDisplacedThresholdFt,
          heLatitudeDeg,
          heLongitudeDeg,
          heElevationFt,
          heHeadingDegT,
          heDisplacedThresholdFt,
        )
      )
      case _ => None
    }
  }

  def verifyAirport(code: Option[String]): Option[AirportCode] = {
    code
      .map(AirportCode)
      .flatMap(x => AirportsRepository.findByID(x))
      .flatMap(x => Some(x.identAirport))
  }
}