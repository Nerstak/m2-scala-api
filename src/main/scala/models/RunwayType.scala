package models

import scala.collection.immutable.HashMap

case class RunwayType(surface: String) extends AnyVal

object RunwayType {
  // List: https://en.wikipedia.org/wiki/Runway#Surface_type_codes
  private val validTypes = HashMap(
    "ASP" -> "ASP",
    "BIT" -> "BIT",
    "COM" -> "COM",
    "CON" -> "CON",
    "COP" -> "COP",
    "DIRT" -> "GRE",
    "GRASS" -> "GRS",
    "GRS" -> "GRS",
    "GRE" -> "GRE",
    "GVL" -> "GVL",
    "GRVL" -> "GVL",
    "GRAVEL" -> "GVL",
    "ICE" -> "ICE",
    "LAT" -> "LAT",
    "MAC" -> "MAC",
    "MATS" -> "PSP",
    "PEM" -> "PEM",
    "PER" -> "PER",
    "PSP" -> "PSP",
    "SAND" -> "SAN",
    "TURF" -> "GRS",
    "WAT" -> "WAT",
    "U" -> "U"
  )

  def verify(string: Option[String]): RunwayType = {
    if(string.isDefined) {
      val key = getFromPartialKey(string.get.toUpperCase())
      key match {
        case Some(value) => RunwayType(value)
        case _ => RunwayType("U")
      }
    } else {
      RunwayType("U")
    }
  }

  private def getFromPartialKey(key: String): Option[String] = {
    validTypes.keySet.find(x => key.startsWith(x))
  }
}
