package models

import repositories.CountriesRepository
import utils.Helpers.{emptyStringOption, stringOptionToList}
import scala.language.implicitConversions

case class Airports(
                     identAirport: AirportCode,
                     countryCode: CountryCode,
                     typeAirport: String,
                     name: String,
                     continentCode: Option[ContinentCode],
                     regionCode: Option[String],
                     latitudeDeg: Double,
                     longitudeDeg: Double,
                     scheduledService: Boolean,
                     elevationFt: Option[Int] = None,
                     municipality: Option[String] = None,
                     gpsCode: Option[String] = None,
                     iataCode: Option[String] = None,
                     localCode: Option[String] = None,
                     homeLink: Option[String] = None,
                     wikiLink: Option[String] = None,
                     keywords: List[String]
                   )

object Airports {
  private def convertBooleanScheduled(s: String) = s == "yes"

  def parseAirport(line: Array[String]): Option[Airports] = {
    (
      line.lift(1).flatMap(x => Some(AirportCode(x))), // Airport Code
      line.lift(2).flatMap(verifyAirportType), // Type Airport
      line.lift(3).flatMap(emptyStringOption), // Name
      line.lift(4).flatMap(_.toDoubleOption), // Latitude Deg
      line.lift(5).flatMap(_.toDoubleOption), // Longitude Deg
      line.lift(6).flatMap(_.toIntOption), // Elevation ft
      line.lift(7).flatMap(ContinentCode.verify),// Continent
      line.lift(8).flatMap(CountryCode.verify),
      line.lift(9).flatMap(emptyStringOption),
      line.lift(10).flatMap(emptyStringOption), // Municipality
      convertBooleanScheduled(line(11)), // Scheduled Service
      line.lift(12).flatMap(emptyStringOption), // GPS Code
      line.lift(13).flatMap(emptyStringOption), // IATA Code
      line.lift(14).flatMap(emptyStringOption), // Local Code
      line.lift(15), // Home Link
      line.lift(16),
      stringOptionToList(line.lift(17)) // Keywords
    ) match {
      case (
        Some(airportCode),
        Some(typeAirport),
        Some(name),
        Some(latitudeDeg),
        Some(longitudeDeg),
        elevationFt,
        continentCode,
        Some(countryCode),
        regionCode,
        municipality,
        scheduledService,
        gpsCode,
        iataCode,
        localCode,
        homeLink,
        wikiLink,
        keywords
        ) => Some(
        Airports(
          airportCode,
          countryCode,
          typeAirport,
          name,
          continentCode,
          regionCode,
          latitudeDeg,
          longitudeDeg,
          scheduledService,
          elevationFt,
          municipality,
          gpsCode,
          iataCode,
          localCode,
          homeLink,
          wikiLink,
          keywords
        )
      )
      case _ => None
    }
  }

  def verifyAirportType(string: String): Option[String] = {
    val validType = List(
      "small_airport",
      "medium_airport",
      "large_airport",
      "heliport",
      "closed",
      "seaplane_base"
    )
    Some(string).filter(validType.contains)
  }

  def verifyCountry(code: Option[String]): Option[CountryCode] = {
    code
      .flatMap(CountryCode.verify)
      .flatMap(x => CountriesRepository.findByID(x))
      .flatMap(x => Some(x.countryCode))
  }
}
