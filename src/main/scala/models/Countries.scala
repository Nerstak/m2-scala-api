package models

import utils.Helpers.{emptyStringOption, stringOptionToList}

case class Countries(
                      countryCode: CountryCode,
                      name: String,
                      continent: ContinentCode,
                      wikiLink: String,
                      keywords: List[String]
                    )

object Countries {
  def parseCountry(line: Array[String]): Option[Countries] = {
    (
      line.lift(1).flatMap(CountryCode.verify), // Country code
      line.lift(2).flatMap(emptyStringOption), // Name
      line.lift(3).flatMap(ContinentCode.verify), // Continent
      line.lift(4), // WikiLink
      stringOptionToList(line.lift(5))// Keywords
    ) match {
      case (
        Some(code),
        Some(name),
        Some(continent),
        Some(wikiLink),
        keywords
        ) => Some(
        Countries(
          code,
          name,
          continent,
          wikiLink,
          keywords
        )
      )
      case _ => None
    }
  }
}