package repositories

import io.getquill.Ord
import models.{AirportCode, Airports, Countries, CountryCode, RunwayID, RunwayType, Runways}
import utils.DatabaseContext

object RunwaysRepository extends Repository[Runways, RunwayID] {
  private lazy val ctx = DatabaseContext.ctx
  import ctx._

  def findRunwaysByAirportId(id:AirportCode): List[Runways] = run(
    query[Runways].filter(_.ident_airport == lift(id)))

  def findAll():List[Runways] = run(query[Runways])

  override def findByID(id: RunwayID): Option[Runways] = run(
    query[Runways]
      .filter(_.runwayId == lift(id))
  ).lastOption

  override def insert(elt: Runways): Long = run(query.insert(lift(elt)))

  override def batchInsert(list: List[Runways]): List[Long] = performIO(
    runIO(
      liftQuery(list)
        .foreach(e => query[Runways].insert(e))
    ).transactional
  )

  override def deleteAll(): Long = run(query[Runways].delete)

  def findMostCommonLatitude(): List[(String, Long)] = run(
    query[Runways]
      .groupBy(_.leIdent)
      .map(x => (x._1 , x._2.size))
      .sortBy(_._2)(Ord.desc)
      .take(10)
  )

  def findRunwaysByCountryId(id:CountryCode): List[RunwayType] = run(
    query[Countries]
      .join(query[Airports])
      .on(_.countryCode == _.countryCode)
      .join(query[Runways])
      .on(_._2.identAirport == _.ident_airport)
      .filter(_._1._1.countryCode == lift(id)))
      .map(_._2.surface)
}
