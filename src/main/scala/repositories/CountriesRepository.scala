package repositories

import io.getquill.Ord
import models.{Airports, Countries, CountryCode}
import service.CountryAirportDto
import utils.DatabaseContext

object CountriesRepository extends Repository[Countries, CountryCode] {
  private lazy val ctx = DatabaseContext.ctx
  import ctx._

  def findCountryByName(name: String): List[Countries] = run(
    query[Countries]
      .filter(_.name == lift(name))
  )

  def findAll():List[Countries] = run(query[Countries])

  def findAllCountriesNameAndCode():List[(String,CountryCode)] = run(query[Countries].map(country => (country.name,country.countryCode)).distinct.sortBy(_._1))

  override def findByID(id: CountryCode): Option[Countries] = run(
    query[Countries]
      .filter(_.countryCode == lift(id))
  ).lastOption

  override def insert(elt: Countries): Long = run(query.insert(lift(elt)))

  override def batchInsert(list: List[Countries]): List[Long] = performIO(
    runIO(
      liftQuery(list)
        .foreach(e => query[Countries].insert(e))
    ).transactional
  )

  override def deleteAll(): Long = run(query[Countries].delete)

  private def findCountriesWithAirportsCount = {
    quote(query[Countries]
      .join(query[Airports])
      .on(_.countryCode == _.countryCode)
      .groupBy(x => x._1.name)
      .map(x => CountryAirportDto(x._1, x._2.size)))
  }

  def findCountriesWithMostAirports(): List[CountryAirportDto] = {
    run(findCountriesWithAirportsCount
      .sortBy(_.count)(Ord.desc)
      .take(10)
    )
  }

  def findCountriesWithLeastAirports(): List[CountryAirportDto] = {
    run(findCountriesWithAirportsCount
      .sortBy(_.count)(Ord.asc)
      .take(10)
    )
  }
}
