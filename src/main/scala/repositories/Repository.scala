package repositories

trait Repository[A, B] {
  def findByID(id: B): Option[A]

  def insert(elt: A): Long

  def batchInsert(list: List[A]): List[Long]

  def deleteAll(): Long
}
