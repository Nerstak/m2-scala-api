package repositories

import models.{AirportCode, Airports, CountryCode, Runways}
import utils.DatabaseContext

object AirportsRepository extends Repository[Airports, AirportCode]{
  private lazy val ctx = DatabaseContext.ctx
  import ctx._

  def findAirportsByName(name: String): List[Airports] = run(query[Airports].filter(_.name == lift(name)))

  def findAirportsByCountryCode(code: CountryCode) : List[Airports] = run(query[Airports]).filter(_.countryCode == code)

  def findAll(): List[Airports] = run(query[Airports])

  override def findByID(id: AirportCode): Option[Airports] = run(
    query[Airports]
      .filter(_.identAirport == lift(id))
  ).lastOption

  override def insert(elt: Airports): Long = run(query.insert(lift(elt)))

  override def batchInsert(list: List[Airports]): List[Long] = performIO(
    runIO(
      liftQuery(list)
        .foreach(e => query[Airports].insert(e))
    ).transactional
  )

  override def deleteAll(): Long = run(query[Airports].delete)

  def findWithRunways(id: CountryCode): List[(Airports, Runways)] = run(
    query[Airports]
      .join(query[Runways])
      .on(_.identAirport == _.ident_airport)
      .filter(_._1.countryCode == lift(id))
  )
}
