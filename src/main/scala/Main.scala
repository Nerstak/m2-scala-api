import models.{Airports, Countries, CountryCode, Runways}
import org.json4s.DefaultFormats
import repositories.{AirportsRepository, CountriesRepository, RunwaysRepository}
import service.{CountryRunWayTypeDto, QueryService, ReportService}
import utils.CSV
import org.json4s.jackson.Serialization.write

import java.net.{InetSocketAddress, Socket}
import scala.collection.mutable.ListBuffer
import scala.io.StdIn.readLine
import scala.util.control.Breaks.{break, breakable}

object Main {
  implicit val formats: DefaultFormats.type = DefaultFormats

  def setUpData(): Boolean = {
    CountriesRepository.deleteAll()
    AirportsRepository.deleteAll()
    RunwaysRepository.deleteAll()
    CountriesRepository.batchInsert(CSV.read("countries.csv", Countries.parseCountry).lines.toList)
    AirportsRepository.batchInsert(CSV.read("airports.csv", Airports.parseAirport).lines.toList)
    RunwaysRepository.batchInsert(CSV.read("runways.csv", Runways.parseRunway).lines.toList)
    true
  }

  def main(args: Array[String]): Unit = {
    println("Data ingestion...")
    setUpData()
    breakable {
      while (true) {
        print(
          """
            |1,Query
            |2,Report
            |3,Quit
            |""".stripMargin)
        if (menuHandler(readLine().trim) == false) break();
      }
    }
  }

  private def queryLoop(): Unit = {
    breakable {
      while (true) {
        print(
          """
            |1,Search by country name
            |2,Search by country code
            |3,<-
            |""".stripMargin)
        if (queryHandler(readLine().trim) == false) break();
      }
    }

  }

 private def reportLoop(): Unit =  breakable {
    while (true) {
      print(
        """
          |1,10 countries with highest number of airports
          |2,10 countries with least number of airports
          |3,Type of runways of per country
          |4,The top 10 most common runway latitude
          |5.<-
          |""".stripMargin)
      if (reportHandler(readLine().trim) == false) break();
    }
  }

  private def menuHandler(option: String) = option match {
    case option: String if "1".equals(option) => queryLoop();
    case option: String if "2".equals(option) => reportLoop();
    case option: String if "3".equals(option) => false;
    case _ => print("Unrecognized input value");
  }

  private def queryHandler(option: String) = option match {
    case option: String if "1".equals(option) => queryByCountryName()
    case option: String if "2".equals(option) => queryByCountryCode()
    case option: String if "3".equals(option) => false
    case _ => print("Unrecognized input value");
  }

  private def reportHandler(option: String) = option match {
    case option: String if "1".equals(option) => {
      println(
        write(
          ReportService
            .findCountriesWithMostAirports()
        )
      )
    }
    case option: String if "2".equals(option) => {
      println(
        write(
          ReportService
            .findCountriesWithLeastAirports()
        )
      )
    }
    case option: String if "3".equals(option) => {
      println(
        write(CountriesRepository
          .findAllCountriesNameAndCode()
          .foldLeft(ListBuffer[CountryRunWayTypeDto]()) (
            (a, l) => a += ReportService
              .findTypeOfRunwaysPerCountry(l._2)
              .getOrElse(
                CountryRunWayTypeDto("Error", List())
              )
          )
        )
      )
    }
    case option: String if "4".equals(option) => println(write(ReportService.findTop10MostCommonLatitude()))
    case option: String if "5".equals(option) => false
    case _ => print("Unrecognized input value");
  }

  private def queryByCountryName(): Unit = {
    print(
      """
        |Please input the country name:
        |""".stripMargin)
    println(
      write(
        QueryService
          .findAirportAndRunwaysByCountryName(
            readLine().trim
          )
          .iterator
          .toList
      )
    )
  }

  private def queryByCountryCode(): Unit = {
    print(
      """
        |Please input the country code:
        |""".stripMargin)
    println(
      write(
        QueryService
          .findAirportAndRunwaysByCountryCode(
            Some(
              CountryCode(readLine().trim)
            )
          ).iterator.toList
      )
    )
  }
}