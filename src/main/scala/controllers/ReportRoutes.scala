package controllers

import cats.Monad
import models.CountryCode
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization.write
import repositories.CountriesRepository
import service.ReportService

object ReportRoutes {
  implicit val formats: DefaultFormats.type = DefaultFormats

  def queryRoutes[F[_] : Monad]: HttpRoutes[F] = {
    val dsl = Http4sDsl[F]
    import dsl._

    HttpRoutes.of[F] {
      case GET -> Root / "countries" / "airports" / value =>
        value match {
          case value: String if value.equals("max") =>
            Ok(write(ReportService.findCountriesWithMostAirports()))
          case value: String if value.equals("min") =>
            Ok(write(ReportService.findCountriesWithLeastAirports()))
          case _ => BadRequest("Selector unknown: 'min' or 'max' needed")
        }
      case GET -> Root / "countries" / countryCode / "runwaysType" =>
        countryCode match {
          case code: String if CountriesRepository.findByID(CountryCode(code)).nonEmpty =>
            Ok(write(ReportService.findTypeOfRunwaysPerCountry(CountryCode(code))))
          case _ => BadRequest("CountryCode not found");
        }
      case GET -> Root / "runways" / "latitude" =>
        Ok(write(ReportService.findTop10MostCommonLatitude()))
    }
  }
}
