package controllers

import cats.Monad
import models.{Airports, CountryCode, Runways}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.dsl.impl.QueryParamDecoderMatcher
import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization.write
import service.QueryService

object QueryRoutes {
  case class PageIngo(totalRecords:Int,currentPage:Int,totalPage:Int)
  case class QueryResponse(data: List[(Airports, Runways)], pageIngo: PageIngo)

  object pageNumMatcher extends  QueryParamDecoderMatcher[Int]("page")
  object countryNameParamMatcher extends QueryParamDecoderMatcher[String]("name")
  object countryCodeParamMatcher extends QueryParamDecoderMatcher[String]("code")
  val recordsPerPage = 10;

  implicit val formats: DefaultFormats.type = DefaultFormats

  def queryRoutes[F[_] : Monad]: HttpRoutes[F] = {
    val dsl = Http4sDsl[F]
    import dsl._

    def handleResponse(pageNum: Int, res: IterableOnce[(Airports, Runways)]) = {
      val size = res.iterator.size
      val totalPage = (size + recordsPerPage - 1) / recordsPerPage
      pageNum match {
        case page: Int if page < 0 =>
          BadRequest("The page number starts from 0");
        case page: Int if page >= totalPage =>
          Ok(
            write(
              QueryResponse(
                res.iterator.slice((totalPage - 1) * recordsPerPage, size).toList,
                PageIngo(size, totalPage, totalPage)
              )
            )
          );
        case page: Int =>
          Ok(
            write(
              QueryResponse(
                res.iterator.slice(page * recordsPerPage, (page + 1) * recordsPerPage).toList,
                PageIngo(size, page, totalPage))))
      }
    }

    HttpRoutes.of[F] {
      case GET -> Root :? countryNameParamMatcher(countryName) +& pageNumMatcher(pageNum) => {
        val res = QueryService.findAirportAndRunwaysByCountryName(countryName)
        handleResponse(pageNum, res)
      }
      case GET ->  Root :? countryCodeParamMatcher(countryCode) +& pageNumMatcher(pageNum) => {
        val res = QueryService.findAirportAndRunwaysByCountryCode(CountryCode.verify(countryCode))
        handleResponse(pageNum, res)
      }
    }
  }
}
