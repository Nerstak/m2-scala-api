import Main.setUpData
import cats.effect._
import controllers.{QueryRoutes, ReportRoutes}
import models.{Airports, Countries, Runways}
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.blaze.server.BlazeServerBuilder
import repositories.{AirportsRepository, CountriesRepository, RunwaysRepository}
import utils.CSV

object Http4sApplication extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = {
    setUpData()

    val apis = Router(
      "/query" -> QueryRoutes.queryRoutes[IO],
      "/report" -> ReportRoutes.queryRoutes[IO]
    ).orNotFound

    BlazeServerBuilder[IO]
      .bindHttp(8080, "localhost")
      .withHttpApp(apis)
      .resource
      .use(_ => IO.never)
      .as(ExitCode.Success)
  }
}