# M2 Scala API 

## Table of contents

- [Information](#Information)

- [Installation](#Installation)

- [Endpoints](#Endpoints)

- [Tests](#Tests)


## Information

### Description

Application to allow users to access data (from [OurAirports](https://ourairports.com/data/)), performing query or requesting reports.

### Technologies used

Build with:
- Scala
- HTTP4S for Rest Server
- Quill for Database interaction

Database:
- SQLite

## Installation

### Setup 

Run `setup.sh` to setup databases (prod & test).

Use the **JDK 11**. The JDK 17 is not supported due to not up-to-date libraries.

### Running the program

In SBT, run `compile; run`. Note that there may be warnings due to libraries not being up-to-date with Java 11 recommendations.

You can choose between the Rest API server, and the Console Interface. 
Note that:
- The Rest server runs on `localhost:8080`
- Data are not optimized for the CLI

## Endpoints

| Action                                                                | Route                                                       | Notes                                                                                                             |
|-----------------------------------------------------------------------|-------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| Report the 10 most common latitude code                               | `localhost:8080/report/runways/latitude`                    |                                                                                                                   |
| Report the type of runways in a country                               | `localhost:8080/report/countries/$COUNTRY_CODE/runwaysType` | $COUNTRY_CODE is the ISO code of the country                                                                      |
| Report the 10 countries with the most or the least number of airports | `localhost:8080/report/countries/airports/$BOUNDARY`        | $BOUNDARY is "max" or "min"                                                                                       |
| Query data on the country                                             | `localhost:8080/query?name=$COUNTRY_NAME&page=$PAGE`        | $COUNTRY_NAME is the name of the country and $PAGE the number of the page (10 elts per page, starting from 0)     |
| Query data on the country                                             | `localhost:8080/query?code=$COUNTRY_CODE&page=$PAGE`        | $COUNTRY_CODE is the ISO code of the country and $PAGE the number of the page (10 elts per page, starting from 0) |

## Tests

For test, run `compile; test`. Tests will take some times as there is a lot of I/O, and are sequential.
